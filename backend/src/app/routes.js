import express from 'express';
import gamesRouter from '../controllers/games/';
import usersRouter from '../controllers/users/';
import authRouter from '../controllers/auth/';
import reviewsRouter from '../controllers/reviews/';
// /api/rest/ routes
const restRoutes = express.Router();

restRoutes.use('/games', gamesRouter);
restRoutes.use('/users', usersRouter);
restRoutes.use('/auth', authRouter);
restRoutes.use('/reviews', reviewsRouter);

const routes = express.Router();
routes.use('/rest', restRoutes);

export default routes;
