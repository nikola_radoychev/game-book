import express from 'express';
import passport from 'passport';
import jwt from 'jsonwebtoken';
import config from '../../config';

const router = express.Router();

router.post('/login', (req, res) => {
  passport.authenticate('local', { session: false }, (authError, user) => {
    if (authError || !user) {
      return res.status(400).json({
        error: 'Unable to authenticate the user.',
      });
    }
    req.login(user, { session: false }, loginError => {
      if (loginError) {
        res.send(loginError);
      }
      const signData = { id: user.id };
      const token = jwt.sign(signData, config.JWT_SECRET);
      return res.json({ token });
    });
  })(req, res);
});

router.get(
  '/profile',
  passport.authenticate('jwt', { session: false }),
  async (req, res) => {
    res.json(req.user);
  }
);

export default router;
