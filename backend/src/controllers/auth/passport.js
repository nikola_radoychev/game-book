import passport from 'passport';
import passportJwt from 'passport-jwt';
import { Strategy as LocalStrategy } from 'passport-local';
import { authenticateUser } from './controller';
import config from '../../config';
import db from '../../models';

const { Strategy: JwtStrategy, ExtractJwt } = passportJwt;

console.log('Load passport strategies...');

passport.use(
  new LocalStrategy(
    {
      usernameField: 'email',
      passwordField: 'password',
    },
    async (email, password, cb) => {
      try {
        const user = await authenticateUser(db, { email, password });
        if (!user) {
          return cb(null, false, {
            message: 'Incorrect email or password.',
          });
        }
        return cb(null, user, { message: 'Logged In Successfully' });
      } catch (err) {
        return cb(err);
      }
    }
  )
);

passport.use(
  new JwtStrategy(
    {
      jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
      secretOrKey: config.JWT_SECRET,
    },
    async (jwtData, cb) => {
      try {
        const user = await db.User.findOne({ _id: jwtData.id }).select(
          'name  email reviews wantsToPlay favouriteGames, role'
        );
        cb(user ? null : 'User not found', user);
      } catch (err) {
        return cb(err);
      }
    }
  )
);
