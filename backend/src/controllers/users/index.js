import express from 'express';
import passport from 'passport';

import db from '../../models';
import {
  createUser,
  changeUserRole,
  fetchAllUsers,
  deleteUser,
} from './controller';

const router = express.Router();

router.post('/create', async (req, res) => {
  try {
    const result = await createUser(db, req.body);
    res.status(201).json(result);
  } catch (err) {
    res.status(400).json({ error: err.toString() });
  }
});

router.patch(
  '/changerole',
  passport.authenticate('jwt', { session: false }),
  async (req, res) => {
    try {
      if (req.user.role !== 'Admin') {
        throw new Error('Access denied');
      }
      const result = await changeUserRole(db, req.body);
      res.status(201).json(result);
    } catch (err) {
      res.status(400).json({ error: err.toString() });
    }
  }
);

router.get(
  '/all',
  passport.authenticate('jwt', { session: false }),
  async (req, res) => {
    try {
      if (req.user.role !== 'Admin') {
        throw new Error('Access denied');
      }
      const result = await fetchAllUsers(db);
      res.status(201).json(result);
    } catch (err) {
      res.status(400).json({ error: err.toString() });
    }
  }
);

router.delete(
  '/:id',
  passport.authenticate('jwt', { session: false }),
  async (req, res) => {
    try {
      if (req.user.role !== 'Admin') {
        throw new Error('Access denied');
      }
      const result = await deleteUser(db, req.params.id);
      res.status(201).json(result);
    } catch (err) {
      res.status(400).json({ error: err.toString() });
    }
  }
);

export default router;
