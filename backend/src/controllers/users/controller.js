import encription from '../auth/encription';

export const createUser = async (db, data) => {
  const { password } = data;
  const passwordSalt = encription.generateSalt();
  const passwordHash = encription.generateHashedPassword(
    passwordSalt,
    password
  );
  const isEmailExists = await db.User.findOne({ email: data.email });
  if (!!isEmailExists) {
    throw new Error('User with this email already exists');
  }
  const users = await db.User.find({ role: 'Admin' });
  const { email, name } = data;
  const user = await db.User.create({
    email,
    name,
    passwordHash,
    passwordSalt,
    role: users.length > 0 ? 'User' : 'Admin',
  });
  return user;
};

export const changeUserRole = async (db, data) => {
  const { id, role } = data;
  const user = await db.User.findOne({ _id: id });
  user.role = role;
  await user.save();
  return user;
};

export const fetchAllUsers = async db => {
  const users = await db.User.find();
  return users;
};

export const deleteUser = async (db, id) => {
  await db.User.findOneAndDelete({ _id: id });
  return 'Deletted successfull';
};
