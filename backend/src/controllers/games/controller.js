import { Mongoose } from 'mongoose';
import { fetchReviewsByGameId } from '../reviews/controller';
const mongoose = new Mongoose();

export const createGame = async (db, data) => {
  const game = await db.Game.create(data);
  return game;
};

export const fetchGames = async (db) => {
  const games = await db.Game.find();
  return games;
};

export const fetchFavouriteGames = async (db, id) => {
  const user = await db.User.findOne({ _id: id });
  const gamesIds = user.favouriteGames.map((x) => mongoose.Types.ObjectId(x));
  const games = await db.Game.find({ _id: { $in: gamesIds } });
  return games;
};

export const fetchWantedGames = async (db, id) => {
  const user = await db.User.findOne({ _id: id });
  const gamesIds = user.wantsToPlay.map((x) => mongoose.Types.ObjectId(x));
  const games = await db.Game.find({ _id: { $in: gamesIds } });
  return games;
};

export const addFavouriteGame = async (db, gameId, userId) => {
  const user = await db.User.findOne({ _id: userId });
  const game = await db.Game.findOne({ _id: gameId.toString() });
  if (user.favouriteGames.map((x) => x.toString()).includes(gameId)) {
    const result = await removeFavouriteGame(db, gameId, userId);
    return result;
  }
  user.favouriteGames = [...user.favouriteGames, game];
  await user.save();
  return user;
};

export const removeFavouriteGame = async (db, gameId, userId) => {
  const user = await db.User.findOne({ _id: userId });
  user.favouriteGames = [...user.favouriteGames].filter(
    (x) => x.toString() !== gameId.toString()
  );
  await user.save();
  return user;
};

export const addWantsToPlayGame = async (db, gameId, userId) => {
  const user = await db.User.findOne({ _id: userId });
  const game = await db.Game.findOne({ _id: gameId.toString() });
  if (user.wantsToPlay.map((x) => x.toString()).includes(gameId)) {
    const result = await removeWantsToPlayGame(db, gameId, userId);
    return result;
  }
  user.wantsToPlay = [...user.wantsToPlay, game];
  await user.save();
  return user;
};

export const removeWantsToPlayGame = async (db, gameId, userId) => {
  const user = await db.User.findOne({ _id: userId });
  user.wantsToPlay = [...user.wantsToPlay].filter(
    (x) => x.toString() !== gameId.toString()
  );
  await user.save();
  return user;
};

export const fetchGameById = async (db, id) => {
  const game = await db.Game.findOne({ _id: id });
  const reviews = await fetchReviewsByGameId(db, { id });
  game.reviews = reviews;
  return game;
};

export const updateGame = async (db, data) => {
  const { id } = data;
  await db.Game.findOneAndUpdate({ _id: id }, data);
  const game = fetchGameById(db, id);
  return game;
};

export const deleteGame = async (db, id) => {
  await db.Game.findOneAndDelete({ _id: id });
  return `The game with id: ${id} is deleted`;
};
