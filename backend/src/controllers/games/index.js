import express from 'express';
import passport from 'passport';
import db from '../../models';
import {
  createGame,
  fetchGames,
  fetchGameById,
  updateGame,
  fetchFavouriteGames,
  addFavouriteGame,
  removeFavouriteGame,
  addWantsToPlayGame,
  removeWantsToPlayGame,
  fetchWantedGames,
  deleteGame,
  getGamesByString,
} from './controller';
const router = express.Router();

router.post(
  '/create',
  passport.authenticate('jwt', { session: false }),
  async (req, res) => {
    try {
      if (req.user.role !== 'User') {
        const result = await createGame(db, req.body);
        res.status(201).json(result);
        return;
      }
      res.status(500).json({ error: 'Insufficient acces' });
    } catch (err) {
      res.status(400).json({ error: err.toString() });
    }
  }
);

router.patch(
  '/update',
  passport.authenticate('jwt', { session: false }),
  async (req, res) => {
    try {
      if (req.user.role !== 'User') {
        const result = await updateGame(db, req.body);
        res.status(201).json(result);
        return;
      }
      res.status(500).json({ error: 'Insufficient acces' });
    } catch (err) {
      res.status(400).json({ error: err.toString() });
    }
  }
);

router.get('/all', async (req, res) => {
  try {
    const games = await fetchGames(db);
    res.status(201).json(games);
  } catch (err) {
    res.status(400).json({ error: err.toString() });
  }
});

router.get(
  '/favourites',
  passport.authenticate('jwt', { session: false }),
  async (req, res) => {
    try {
      const games = await fetchFavouriteGames(db, req.user._id);
      res.status(201).json(games);
    } catch (err) {
      res.status(400).json({ error: err.toString() });
    }
  }
);

router.get(
  '/wantstoplay',
  passport.authenticate('jwt', { session: false }),
  async (req, res) => {
    try {
      const games = await fetchWantedGames(db, req.user._id);
      res.status(201).json(games);
    } catch (err) {
      res.status(400).json({ error: err.toString() });
    }
  }
);

router.post(
  '/add/favourite',
  passport.authenticate('jwt', { session: false }),
  async (req, res) => {
    try {
      const userId = req.user._id;
      const { gameId } = req.body;
      const games = await addFavouriteGame(db, gameId, userId);
      res.status(201).json(games);
    } catch (err) {
      res.status(400).json({ error: err.toString() });
    }
  }
);

router.post(
  '/remove/favourite',
  passport.authenticate('jwt', { session: false }),
  async (req, res) => {
    try {
      const userId = req.user._id;
      const { gameId } = req.body;
      const games = await removeFavouriteGame(db, gameId, userId);
      res.status(201).json(games);
    } catch (err) {
      res.status(400).json({ error: err.toString() });
    }
  }
);

router.post(
  '/add/wantstoplay',
  passport.authenticate('jwt', { session: false }),
  async (req, res) => {
    try {
      const userId = req.user._id;
      const { gameId } = req.body;
      const games = await addWantsToPlayGame(db, gameId, userId);
      res.status(201).json(games);
    } catch (err) {
      res.status(400).json({ error: err.toString() });
    }
  }
);

router.post(
  '/remove/wantstoplay',
  passport.authenticate('jwt', { session: false }),
  async (req, res) => {
    try {
      const userId = req.user._id;
      const { gameId } = req.body;
      const games = await removeWantsToPlayGame(db, gameId, userId);
      res.status(201).json(games);
    } catch (err) {
      res.status(400).json({ error: err.toString() });
    }
  }
);

router.get('/:id', async (req, res) => {
  try {
    const { id } = req.params;
    const game = await fetchGameById(db, id);
    res.status(201).json(game);
  } catch (err) {
    res.status(400).json({ error: err.toString() });
  }
});

router.delete(
  '/:id',
  passport.authenticate('jwt', { session: false }),
  async (req, res) => {
    try {
      if (req.user.role === 'User') {
        res.status(500).json({ error: 'Insufficient access' });
        return;
      }
      const { id } = req.params;
      const game = await deleteGame(db, id);
      res.status(201).json(game);
    } catch (err) {
      res.status(400).json({ error: err.toString() });
    }
  }
);

export default router;
