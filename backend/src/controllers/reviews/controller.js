import { Mongoose } from 'mongoose';
const mongoose = new Mongoose();

export const createReview = async (db, userId, { gameId, content }) => {
  const game = await db.Game.findOne({ _id: gameId });
  const creator = await db.User.findOne({ _id: userId });
  const review = await db.Review.create({ content, game, creator });
  return review;
};

export const updateReview = async (db, { reviewId, content }) => {
  const review = await db.Review.findOne({ _id: reviewId });
  review.content = content;
  await review.save();
  return review;
};

export const deleteReview = async (db, { id }) => {
  await db.Review.findOneAndDelete({ _id: id });
  return 'The review is deleted';
};

export const fetchReviewsByGameId = async (db, { id }) => {
  const reviews = await db.Review.find({ game: id })
    .select('content creator game')
    .populate('creator')
    .populate('game');
  return reviews;
};
