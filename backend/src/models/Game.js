import mongoose from 'mongoose';
import { Schema } from 'mongoose';

const gameSchema = new Schema({
  name: {
    type: String,
    required: true,
  },
  description: {
    type: String,
    required: true,
  },
  ownerCompany: {
    type: String,
    required: true,
  },
  imageUrl: {
    type: String,
    required: true,
  },
  trailerUrl: {
    type: String,
    required: true,
  },
  reviews: [
    {
      type: Schema.Types.ObjectId,
      ref: 'Review',
    },
  ],
});

export default mongoose.model('Game', gameSchema);
