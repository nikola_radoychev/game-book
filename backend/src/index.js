import express from 'express';
import bodyParser from 'body-parser';
import cors from 'cors';
import routes from './app/routes';
import './controllers/auth/passport';

require('./database/database')();

const port = 5050;
const app = express();

app.use(bodyParser.json());
app.use((req, res, next) => {
  res.setHeader('Access-Control-Allow-Origin', '*');
  res.setHeader(
    'Access-Control-Allow-Methods',
    'OPTIONS, GET, POST, PUT, PATCH, DELETE'
  );
  res.setHeader('Access-Control-Allow-Headers', 'Content-Type, Authorization');
  next();
});

app.use(routes);
// app.use(cors);

// General error handling
app.use((error, req, res, next) => {
  const status = error.statusCode || 500;
  const message = error.message;
  res.status(status).json({
    message: message,
  });
  next();
});

app.listen(port, () => {
  console.log(`Server is listening on port: ${port}...`);
});
