# Game Book - Angular

## Components

- Backend API (NodeJS + Express)
- Frontend (Angular + NgRx(rxjs))

## Weapons of Choice

- Programming language: **JavaScript/TypeScript** (ES6+) (NodeJS runtime 11+) (Babel)
- Package manager: **yarn**
- Database: **MongoDB**
  - ORM - Mongoose
- UI - Angular material + pure css

## Project configuration

- Install dependencies
  - /backend yarn install --frozen-lockfile
  - /frontend yarn install --frozen-lockfile
- Start backend development server: yarn run server
  - port: 5050
- Start frontend development server: ng serve
- Presentation - implemented in the project: route - '/about'

## App configuration

- Admin user
  - The first registered user
  - The first registered user if there isn't a user with 'Admin' role
  - The one with edited role by another Admin
