import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';
import { AboutComponent } from './about/about.component';

const appRoutes: Routes = [
  {
    path: '',
    redirectTo: '/games',
    pathMatch: 'full',
  },
  {
    path: 'games',
    loadChildren: () => {
      return import('./games/games.module').then(
        (module) => module.GamesModule
      );
    },
  },
  {
    path: 'auth',
    loadChildren: () => {
      return import('./auth/auth.module').then((module) => module.AuthModule);
    },
  },
  {
    path: 'users',
    loadChildren: () => {
      return import('./users/users.module').then(
        (module) => module.UsersModule
      );
    },
  },
  {
    path: 'about',
    component: AboutComponent,
  },
];

@NgModule({
  imports: [
    RouterModule.forRoot(appRoutes, {
      preloadingStrategy: PreloadAllModules,
      initialNavigation: 'enabled',
    }),
  ],
  exports: [RouterModule],
})
export class AppRoutingModule {}
