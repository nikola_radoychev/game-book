import { UserResponseData } from '../auth/store/auth.effects';

export const mapDbToUser = (userRes: UserResponseData, token: string) => {
  return { ...userRes, id: userRes._id, _id: undefined, token };
};
