import { Game } from '../games/games.model';
import { GamesTable } from '../games/games-table/games.table.component';
import {
  GameResponseData,
  ReviewResponseData,
} from '../games/store/games.effects';
import * as GameActions from '../games/store/games.actions';
import { Review } from '../games/review.model';

const mapDbToReview = (review: ReviewResponseData): Review => {
  return {
    id: review._id,
    content: review.content,
    creator: review.creator.name,
  };
};

export const mapDbToGame = (gameRes: GameResponseData) => {
  return {
    ...gameRes,
    id: gameRes._id,
    _id: undefined,
    reviews: gameRes.reviews.map(mapDbToReview),
  };
};

export const mapGameToTableData = (game: Game): GamesTable => {
  return {
    ...game,
    action: new GameActions.DeleteGameStarted(game.id),
    image: game.imageUrl,
    imageUrl: undefined,
  };
};

export const mapFavouriteGameToTableData = (game: Game): GamesTable => {
  return {
    ...game,
    action: new GameActions.RemoveFavGameStarted(game.id),
    image: game.imageUrl,
    imageUrl: undefined,
  };
};

export const mapWantedGameToTableData = (game: Game): GamesTable => {
  return {
    ...game,
    action: new GameActions.RemoveTryGameStarted(game.id),
    image: game.imageUrl,
    imageUrl: undefined,
  };
};
