import { Component, OnDestroy, OnInit } from '@angular/core';
import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import { Observable, Subscription } from 'rxjs';
import { map, shareReplay } from 'rxjs/operators';
import { ActivatedRoute, Router } from '@angular/router';
import { Store } from '@ngrx/store';
import { User, UserRole } from '../auth/users.model';
import * as AuthActions from '../auth/store/auth.actions';
import { AppState } from '../store/app.reducer';

interface NavOptions {
  route: string;
  name: string;
  access: Array<UserRole | 'Public' | 'Auth'>;
  icon: string;
}

const navOptions: Array<NavOptions> = [
  {
    route: '/',
    name: 'Home',
    access: ['Public'],
    icon: 'home',
  },
  {
    route: 'games/new',
    name: 'Add new',
    access: ['Moderator', 'Admin'],
    icon: 'add',
  },
  {
    route: '/games/all',
    name: 'All games',
    access: ['Moderator', 'Admin'],
    icon: 'sports_esports',
  },
  {
    route: '/games/favourites',
    name: 'Favourites',
    access: ['User', 'Moderator', 'Admin'],
    icon: 'star',
  },
  {
    route: '/games/to-try',
    name: 'To try',
    access: ['User', 'Moderator', 'Admin'],
    icon: 'flag',
  },
  {
    route: '/users',
    name: 'Users',
    access: ['Admin'],
    icon: 'people',
  },
  {
    route: '/auth',
    name: 'Sign in',
    access: ['Public', 'Auth'],
    icon: 'login',
  },
  {
    route: '/about',
    name: 'About',
    access: ['Public'],
    icon: 'info',
  },
];
@Component({
  selector: 'app-main-nav',
  templateUrl: './main-nav.component.html',
  styleUrls: ['./main-nav.component.scss'],
})
export class MainNavComponent implements OnInit, OnDestroy {
  subscription: Subscription;
  public navOptions: Array<NavOptions>;
  public user: User;

  isHandset$: Observable<boolean> = this.breakpointObserver
    .observe(Breakpoints.Handset)
    .pipe(
      map((result) => result.matches),
      shareReplay()
    );

  constructor(
    private breakpointObserver: BreakpointObserver,
    private router: Router,
    private store: Store<AppState>,
    private route: ActivatedRoute
  ) {}

  ngOnInit(): void {
    this.subscription = this.store.select('auth').subscribe((state) => {
      this.user = state.user;
      this.navOptions = navOptions.filter((navOption) => {
        if (state.user?.role) {
          return (
            (navOption.access.includes(state.user?.role) ||
              navOption.access.includes('Public')) &&
            !navOption.access.includes('Auth')
          );
        } else {
          return navOption.access.includes('Public');
        }
      });
    });
  }

  onSelectRoute(route: string): void {
    this.router.navigate([route], { relativeTo: this.route });
  }

  onLogout(): void {
    this.store.dispatch(new AuthActions.Logout());
  }

  ngOnDestroy(): void {
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
  }
}
