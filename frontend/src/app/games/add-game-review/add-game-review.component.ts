import {
  Component,
  EventEmitter,
  Input,
  OnInit,
  Output,
  ViewChild,
} from '@angular/core';
import { NgForm } from '@angular/forms';
import { Store } from '@ngrx/store';
import { AppState } from '../../store/app.reducer';
import * as GamesActions from '../store/games.actions';

@Component({
  selector: 'app-add-game-review',
  templateUrl: 'add-game-review.component.html',
  styleUrls: ['add-game-review.component.scss'],
})
export class AddGameReviewComponent implements OnInit {
  @ViewChild('f', { static: false }) reviewForm: NgForm;
  @Input() gameId: string;
  @Output() dialogClosed = new EventEmitter<void>();

  constructor(private store: Store<AppState>) {}

  ngOnInit(): void {}

  getErrorMessage() {}

  onCloseForm() {
    this.dialogClosed.emit();
  }

  onSubmit() {
    if (!this.reviewForm.valid) {
      return;
    }
    const { content } = this.reviewForm.value;

    this.store.dispatch(
      new GamesActions.AddGameReviewStarted({
        gameId: this.gameId,
        content,
      })
    );

    this.reviewForm.reset();
    this.onCloseForm();
  }
}
