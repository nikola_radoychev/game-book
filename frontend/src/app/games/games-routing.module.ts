import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { GameListComponent } from './game-list/game-list.component';
import { GameComponent } from './game/game.component';
import { GamesComponent } from './games.component';
import { GamesTableComponent } from './games-table/games.table.component';
import { ManageGameComponent } from './manage-game/manage-game.component';
import { AuthGuard } from '../auth/guards/auth.guard';

const routes: Routes = [
  {
    path: '',
    component: GamesComponent,
    children: [
      { path: '', component: GameListComponent },
      {
        path: 'all',
        component: GamesTableComponent,
        canActivate: [AuthGuard],
      },
      {
        path: 'favourites',
        component: GamesTableComponent,
        canActivate: [AuthGuard],
      },
      {
        path: 'to-try',
        component: GamesTableComponent,
        canActivate: [AuthGuard],
      },
      { path: 'new', component: ManageGameComponent },
      { path: ':id', component: GameComponent },
      {
        path: ':id/edit',
        component: ManageGameComponent,
      },
    ],
  },
];
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class GamesRoutingModule {}
