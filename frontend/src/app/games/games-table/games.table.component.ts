import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Store } from '@ngrx/store';
import { Subscription } from 'rxjs';
import {
  mapFavouriteGameToTableData,
  mapGameToTableData,
  mapWantedGameToTableData,
} from 'src/app/mappers/games';
import { TableService } from 'src/app/shared/table/table.service';
import { AppState } from 'src/app/store/app.reducer';
import { Game } from '../games.model';
import * as GameActions from '../store/games.actions';

export interface GamesTable extends Game {
  action: GameActions.GamesActions;
  image: string;
}

const getTableServiceData = (
  routerPath: string
): {
  tableTitle: 'All Games' | 'Favourite Games' | 'Games to play';
  stateProp: 'games' | 'favGames' | 'tryGames';
  mapper: (game: Game) => GamesTable;
} => {
  switch (routerPath) {
    case 'favourites':
      return {
        tableTitle: 'Favourite Games',
        stateProp: 'favGames',
        mapper: mapFavouriteGameToTableData,
      };
    case 'to-try':
      return {
        tableTitle: 'Games to play',
        stateProp: 'tryGames',
        mapper: mapWantedGameToTableData,
      };
    default:
      return {
        tableTitle: 'All Games',
        stateProp: 'games',
        mapper: mapGameToTableData,
      };
  }
};

export const gameTableColumns = [
  'image',
  'id',
  'name',
  'ownerCompany',
  'action',
];
@Component({
  selector: 'app-games-table',
  templateUrl: './games.table.component.html',
  styleUrls: ['./games.table.component.scss'],
})
export class GamesTableComponent implements OnInit, OnDestroy {
  games: Array<Game>;
  subscription: Subscription;
  constructor(
    private store: Store<AppState>,
    private tableService: TableService,
    private route: ActivatedRoute
  ) {}

  ngOnInit(): void {
    setTimeout(() => {
      this.subscription = this.store.select('games').subscribe((state) => {
        const { tableTitle, stateProp, mapper } = getTableServiceData(
          this.route.routeConfig.path
        );
        this.tableService.setTableData(
          state[stateProp].map(mapper),
          gameTableColumns,
          tableTitle
        );
        this.games = state.games;
      });
    }, 0);
  }

  ngOnDestroy(): void {
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
  }
}
