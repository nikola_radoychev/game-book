import { Component, OnDestroy, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { Subscription } from 'rxjs';
import { AppState } from '../store/app.reducer';
import * as GameActions from './store/games.actions';

@Component({
  selector: 'app-games',
  templateUrl: './games.component.html',
  styleUrls: ['./games.component.scss'],
})
export class GamesComponent implements OnInit, OnDestroy {
  subscription: Subscription;
  constructor(private store: Store<AppState>) {}

  ngOnInit(): void {
    this.store.dispatch(new GameActions.FetchGamesStarted());
    this.subscription = this.store.select('auth').subscribe((state) => {
      if (state.user?.email) {
        this.store.dispatch(new GameActions.FetchFavGamesStarted());
        this.store.dispatch(new GameActions.FetchTryGamesStarted());
      }
    });
  }

  ngOnDestroy(): void {
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
  }
}
