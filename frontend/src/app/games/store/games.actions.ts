import { Action } from '@ngrx/store';
import { Game } from '../games.model';

export const FETCH_GAMES_STARTED = '[Games] Fetch games started';
export class FetchGamesStarted implements Action {
  readonly type = FETCH_GAMES_STARTED;
}

export const FETCH_GAMES_SUCCEEDED = '[Games] Fetch games succeeded';
export class FetchGamesSucceeded implements Action {
  readonly type = FETCH_GAMES_SUCCEEDED;
  constructor(public payload: Array<Game>) {}
}

export const FETCH_GAME_STARTED = '[Games] Fetch game started';
export class FetchGameStarted implements Action {
  readonly type = FETCH_GAME_STARTED;
  constructor(public payload: string) {}
}

export const FETCH_GAME_SUCCEEDED = '[Games] Fetch game succeeded';
export class FetchGameSucceeded implements Action {
  readonly type = FETCH_GAME_SUCCEEDED;
  constructor(public payload: Game) {}
}

export const MANAGE_GAME_STARTED = '[Games] Add game started';
export class ManageGameStarted implements Action {
  readonly type = MANAGE_GAME_STARTED;
  constructor(
    public payload: {
      game: Game;
      mode: 'update' | 'create';
      requestMethod: 'post' | 'patch';
    }
  ) {}
}

export const MANAGE_GAME_SUCCEEDED = '[Games] Manage game succeeded';
export class ManageGameSucceeded implements Action {
  readonly type = MANAGE_GAME_SUCCEEDED;
}

export const FETCH_FAV_GAMES_STARTED = '[Games] Fetch favorite games started';
export class FetchFavGamesStarted implements Action {
  readonly type = FETCH_FAV_GAMES_STARTED;
}

export const FETCH_FAV_GAMES_SUCCEEDED =
  '[Games] Fetch favorite games succeeded';
export class FetchFavGamesSucceeded implements Action {
  readonly type = FETCH_FAV_GAMES_SUCCEEDED;
  constructor(public payload: Array<Game>) {}
}

export const FETCH_TRY_GAMES_STARTED = '[Games] Fetch wanted games started';
export class FetchTryGamesStarted implements Action {
  readonly type = FETCH_TRY_GAMES_STARTED;
}

export const FETCH_TRY_GAMES_SUCCEEDED = '[Games] Fetch wanted games succeeded';
export class FetchTryGamesSucceeded implements Action {
  readonly type = FETCH_TRY_GAMES_SUCCEEDED;
  constructor(public payload: Array<Game>) {}
}

export const ADD_FAV_GAME_STARTED = '[Games] Add favorite game started';
export class AddFavGameStarted implements Action {
  readonly type = ADD_FAV_GAME_STARTED;
  constructor(public payload: string) {}
}

export const REMOVE_FAV_GAME_STARTED = '[Games] Remove favorite game started';
export class RemoveFavGameStarted implements Action {
  readonly type = REMOVE_FAV_GAME_STARTED;
  constructor(public payload: string) {}
}
export const ADD_TRY_GAME_STARTED = '[Games] Add wanted game started';
export class AddTryGameStarted implements Action {
  readonly type = ADD_TRY_GAME_STARTED;
  constructor(public payload: string) {}
}

export const REMOVE_TRY_GAME_STARTED = '[Games] Remove wanted game started';
export class RemoveTryGameStarted implements Action {
  readonly type = REMOVE_TRY_GAME_STARTED;
  constructor(public payload: string) {}
}

export const DELETE_GAME_STARTED = '[Games] Delete game started';
export class DeleteGameStarted implements Action {
  readonly type = DELETE_GAME_STARTED;
  constructor(public payload: string) {}
}

export const ADD_GAME_REVIEW_STARTED = '[Games] Add game review started';
export class AddGameReviewStarted implements Action {
  readonly type = ADD_GAME_REVIEW_STARTED;
  constructor(public payload: { gameId: string; content: string }) {}
}

export const REMOVE_GAME_REVIEW_STARTED = '[Games] Remove game review started';
export class RemoveGameReviewStarted implements Action {
  readonly type = REMOVE_GAME_REVIEW_STARTED;
  constructor(public payload: { gameId: string; reviewId: string }) {}
}

export type GamesActions =
  | FetchGamesStarted
  | FetchGamesSucceeded
  | FetchGameStarted
  | FetchGameSucceeded
  | ManageGameStarted
  | ManageGameSucceeded
  | DeleteGameStarted
  | FetchFavGamesStarted
  | FetchFavGamesSucceeded
  | FetchTryGamesStarted
  | FetchTryGamesSucceeded
  | AddFavGameStarted
  | RemoveFavGameStarted
  | AddTryGameStarted
  | RemoveTryGameStarted
  | AddGameReviewStarted
  | RemoveGameReviewStarted;
