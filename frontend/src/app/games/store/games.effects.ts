import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { of } from 'rxjs';
import { catchError, map, switchMap, tap } from 'rxjs/operators';
import { mapDbToGame } from 'src/app/mappers/games';
import { environment } from 'src/environments/environment';
import * as ErrorsActions from '../../errors/store/errors.actions';
import * as GamesActions from './games.actions';

export interface GameResponseData {
  reviews: Array<ReviewResponseData>;
  _id: string;
  name: string;
  ownerCompany: string;
  imageUrl: string;
  description: string;
  trailerUrl: string;
}

export interface ReviewResponseData {
  _id: string;
  content: string;
  creator: {
    _id: string;
    name: string;
  };
}

const handleError = (errorRes) => {
  const errorMessage = 'Server operation failed!';
  if (
    (!errorRes.error && !errorRes.error.error) ||
    (typeof errorRes.error !== 'string' && !errorRes.error.error)
  ) {
    return of(new ErrorsActions.SetError(errorMessage));
  }
  return of(
    new ErrorsActions.SetError(errorRes.error?.error || errorRes.error)
  );
};

const gamesUrl = `${environment.apiUrl}/games`;

@Injectable()
export class GamesEffects {
  @Effect()
  fetchGames = this.actions.pipe(
    ofType(GamesActions.FETCH_GAMES_STARTED),
    switchMap((_: GamesActions.FetchGamesStarted) => {
      return this.http.get(`${gamesUrl}/all`).pipe(
        catchError(handleError),
        map(
          (data: Array<GameResponseData>) =>
            new GamesActions.FetchGamesSucceeded(data.map(mapDbToGame))
        ),
        catchError(handleError)
      );
    })
  );

  @Effect()
  fetchGame = this.actions.pipe(
    ofType(GamesActions.FETCH_GAME_STARTED),
    switchMap((reqData: GamesActions.FetchGameStarted) => {
      return this.http.get(`${gamesUrl}/${reqData.payload}`).pipe(
        map(
          (resData: GameResponseData) =>
            new GamesActions.FetchGameSucceeded(mapDbToGame(resData))
        ),
        catchError(handleError)
      );
    })
  );

  @Effect()
  manageGame = this.actions.pipe(
    ofType(GamesActions.MANAGE_GAME_STARTED),
    switchMap((reqData: GamesActions.ManageGameStarted) => {
      return this.http[reqData.payload.requestMethod](
        `${gamesUrl}/${reqData.payload.mode}`,
        reqData.payload.game
      ).pipe(
        map(() => new GamesActions.ManageGameSucceeded()),
        catchError(handleError)
      );
    })
  );

  @Effect()
  manageGameSucceeded = this.actions.pipe(
    ofType(GamesActions.MANAGE_GAME_SUCCEEDED),
    map(() => new GamesActions.FetchGamesStarted()),
    catchError(handleError)
  );

  @Effect({ dispatch: false })
  authRedirect = this.actions.pipe(
    ofType(GamesActions.MANAGE_GAME_SUCCEEDED),
    tap((_: GamesActions.ManageGameSucceeded) => {
      this.router.navigate(['/games']);
    })
  );

  @Effect()
  removeReview = this.actions.pipe(
    ofType(GamesActions.REMOVE_GAME_REVIEW_STARTED),
    switchMap((reqData: GamesActions.RemoveGameReviewStarted) => {
      return this.http
        .delete(`${environment.apiUrl}/reviews/${reqData.payload.reviewId}`)
        .pipe(
          map(() => new GamesActions.FetchGameStarted(reqData.payload.gameId)),
          catchError(handleError)
        );
    })
  );

  @Effect()
  addReview = this.actions.pipe(
    ofType(GamesActions.ADD_GAME_REVIEW_STARTED),
    switchMap((reqData: GamesActions.AddGameReviewStarted) => {
      return this.http
        .post(`${environment.apiUrl}/reviews/create`, reqData.payload)
        .pipe(
          map(() => new GamesActions.FetchGameStarted(reqData.payload.gameId)),
          catchError(handleError)
        );
    })
  );

  @Effect()
  fetchFavGames = this.actions.pipe(
    ofType(GamesActions.FETCH_FAV_GAMES_STARTED),
    switchMap((_: GamesActions.FetchFavGamesStarted) => {
      return this.http.get(`${gamesUrl}/favourites`).pipe(
        map(
          (data: Array<GameResponseData>) =>
            new GamesActions.FetchFavGamesSucceeded(data.map(mapDbToGame))
        ),
        catchError(handleError)
      );
    })
  );

  @Effect()
  fetchTryGames = this.actions.pipe(
    ofType(GamesActions.FETCH_FAV_GAMES_STARTED),
    switchMap((_: GamesActions.FetchFavGamesStarted) => {
      return this.http.get(`${gamesUrl}/wantstoplay`).pipe(
        map(
          (data: Array<GameResponseData>) =>
            new GamesActions.FetchTryGamesSucceeded(data.map(mapDbToGame))
        ),
        catchError(handleError)
      );
    })
  );

  @Effect()
  addFavGame = this.actions.pipe(
    ofType(GamesActions.ADD_FAV_GAME_STARTED),
    switchMap((data: GamesActions.AddFavGameStarted) => {
      return this.http
        .post(`${gamesUrl}/add/favourite`, { gameId: data.payload })
        .pipe(
          map(() => new GamesActions.FetchFavGamesStarted()),
          catchError(handleError)
        );
    })
  );

  @Effect()
  removeFavGame = this.actions.pipe(
    ofType(GamesActions.REMOVE_FAV_GAME_STARTED),
    switchMap((data: GamesActions.RemoveFavGameStarted) => {
      return this.http
        .post(`${gamesUrl}/remove/favourite`, { gameId: data.payload })
        .pipe(
          map(() => new GamesActions.FetchFavGamesStarted()),
          catchError(handleError)
        );
    })
  );

  @Effect()
  addTryGame = this.actions.pipe(
    ofType(GamesActions.ADD_TRY_GAME_STARTED),
    switchMap((data: GamesActions.AddTryGameStarted) => {
      return this.http
        .post(`${gamesUrl}/add/wantstoplay`, { gameId: data.payload })
        .pipe(
          map(() => new GamesActions.FetchFavGamesStarted()),
          catchError(handleError)
        );
    })
  );

  @Effect()
  removeTryGame = this.actions.pipe(
    ofType(GamesActions.REMOVE_TRY_GAME_STARTED),
    switchMap((data: GamesActions.RemoveTryGameStarted) => {
      return this.http
        .post(`${gamesUrl}/remove/wantstoplay`, { gameId: data.payload })
        .pipe(
          map(() => new GamesActions.FetchFavGamesStarted()),
          catchError(handleError)
        );
    })
  );

  @Effect()
  deleteGame = this.actions.pipe(
    ofType(GamesActions.DELETE_GAME_STARTED),
    switchMap((data: GamesActions.DeleteGameStarted) => {
      return this.http
        .delete(`${gamesUrl}/${data.payload}`)
        .pipe(map(() => new GamesActions.FetchGamesStarted()));
    }),
    catchError(handleError)
  );

  constructor(
    private actions: Actions,
    private http: HttpClient,
    private router: Router
  ) {}
}
