import { Game } from '../games.model';
import * as GamesActions from './games.actions';

export interface State {
  games: Array<Game>;
  favGames: Array<Game>;
  tryGames: Array<Game>;
  selectedGame: Game;
  loading: boolean;
}

const initialState = {
  games: [],
  favGames: [],
  tryGames: [],
  loading: false,
  selectedGame: null,
};

export const gamesReducer = (
  state: State = initialState,
  action: GamesActions.GamesActions
): State => {
  switch (action.type) {
    case GamesActions.FETCH_GAMES_STARTED:
    case GamesActions.FETCH_GAME_STARTED:
    case GamesActions.DELETE_GAME_STARTED:
    case GamesActions.FETCH_FAV_GAMES_STARTED:
    case GamesActions.FETCH_TRY_GAMES_STARTED:
    case GamesActions.ADD_FAV_GAME_STARTED:
    case GamesActions.REMOVE_FAV_GAME_STARTED:
    case GamesActions.ADD_TRY_GAME_STARTED:
    case GamesActions.REMOVE_TRY_GAME_STARTED: {
      return { ...state, loading: true };
    }
    case GamesActions.FETCH_GAMES_SUCCEEDED: {
      return { ...state, games: action.payload, loading: false };
    }
    case GamesActions.FETCH_GAME_SUCCEEDED: {
      return { ...state, selectedGame: action.payload, loading: false };
    }
    case GamesActions.FETCH_FAV_GAMES_SUCCEEDED: {
      return { ...state, favGames: action.payload, loading: false };
    }
    case GamesActions.FETCH_TRY_GAMES_SUCCEEDED: {
      return { ...state, tryGames: action.payload, loading: false };
    }
    default:
      return state;
  }
};
