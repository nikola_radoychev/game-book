import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import { Store } from '@ngrx/store';
import { Subscription } from 'rxjs';
import { AppState } from 'src/app/store/app.reducer';
import { Game } from '../games.model';
import * as GameActions from '../store/games.actions';

@Component({
  selector: 'app-game',
  templateUrl: './game.component.html',
  styleUrls: ['./game.component.scss'],
})
export class GameComponent implements OnInit, OnDestroy {
  game: Game;
  routeSub: Subscription;
  gameSub: Subscription;
  userSub: Subscription;
  userRole: string;

  constructor(private route: ActivatedRoute, private store: Store<AppState>) {}

  ngOnInit(): void {
    this.routeSub = this.route.params.subscribe((params: Params) => {
      this.store.dispatch(new GameActions.FetchGameStarted(params.id));
    });

    this.gameSub = this.store.select('games').subscribe((state) => {
      this.game = state.selectedGame;
    });

    this.userSub = this.store.select('auth').subscribe((state) => {
      this.userRole = state.user.role;
    });
  }

  onRemoveReview(gameId: string, reviewId: string): void {
    this.store.dispatch(
      new GameActions.RemoveGameReviewStarted({ gameId, reviewId })
    );
  }

  checkReviewOwner(): boolean {
    return this.userRole && this.userRole !== 'User';
  }

  ngOnDestroy(): void {
    if (this.routeSub) {
      this.routeSub.unsubscribe();
    }
    if (this.gameSub) {
      this.gameSub.unsubscribe();
    }
    if (this.userSub) {
      this.userSub.unsubscribe();
    }
  }
}
