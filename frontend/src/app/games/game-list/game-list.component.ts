import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import { Component, OnDestroy, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { Observable, Subscription } from 'rxjs';
import { map } from 'rxjs/operators';
import { AppState } from 'src/app/store/app.reducer';
import { Game } from '../games.model';

export interface GameCard extends Game {
  cols: number;
  rows: number;
}
@Component({
  selector: 'app-game-list',
  templateUrl: './game-list.component.html',
  styleUrls: ['./game-list.component.scss'],
})
export class GameListComponent implements OnInit, OnDestroy {
  games: Observable<Array<GameCard>>;

  gamesSub: Subscription;
  /** Based on the screen size, switch from standard to one column per row */
  constructor(
    private breakpointObserver: BreakpointObserver,
    private store: Store<AppState>
  ) {}

  ngOnInit(): void {
    this.gamesSub = this.store.select('games').subscribe((state) => {
      this.games = this.breakpointObserver.observe(Breakpoints.Handset).pipe(
        map(({ matches }) => {
          if (matches) {
            return [...state.games].map((game) => ({
              ...game,
              cols: 2,
              rows: 1,
            }));
          }

          return [...state.games].map((game) => ({
            ...game,
            cols: 1,
            rows: 1,
          }));
        })
      );
    });
  }

  ngOnDestroy(): void {
    if (this.gamesSub) {
      this.gamesSub.unsubscribe();
    }
  }
}
