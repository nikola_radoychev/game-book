import { Review } from './review.model';

export interface Game {
  reviews: Array<Review>;
  id: string;
  name: string;
  ownerCompany: string;
  imageUrl: string;
  description: string;
  trailerUrl: string;
}
