import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { SharedModule } from '../shared/shared.module';
import { GameItemComponent } from './game-item/game-item.component';
import { GamesRoutingModule } from './games-routing.module';
import { GamesComponent } from './games.component';
import { GamesTableComponent } from './games-table/games.table.component';
import { GameListComponent } from './game-list/game-list.component';
import { GameComponent } from './game/game.component';
import { AddGameReviewComponent } from './add-game-review/add-game-review.component';
import { ManageGameComponent } from './manage-game/manage-game.component';
@NgModule({
  declarations: [
    GamesComponent,
    GamesTableComponent,
    GameItemComponent,
    GameListComponent,
    GameComponent,
    AddGameReviewComponent,
    ManageGameComponent,
  ],
  imports: [RouterModule, GamesRoutingModule, SharedModule],
})
export class GamesModule {}
