import { Component, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { Store } from '@ngrx/store';
import { Subscription } from 'rxjs';
import { map } from 'rxjs/operators';
import { AppState } from 'src/app/store/app.reducer';
import { Game } from '../games.model';

import * as GamesActions from '../store/games.actions';

@Component({
  selector: 'app-manage-game',
  templateUrl: './manage-game.component.html',
  styleUrls: ['./manage-game.component.scss'],
})
export class ManageGameComponent implements OnInit {
  manageMode: 'update' | 'create' = 'create';
  requestMethod: 'patch' | 'post' = 'post';
  id: string;
  @ViewChild('f', { static: false }) gameForm: NgForm;
  gameSubscription: Subscription;
  routeSubscription: Subscription;
  isPreviewMode: boolean = false;

  constructor(
    private store: Store<AppState>,
    private router: Router,
    private route: ActivatedRoute
  ) {}

  ngOnInit(): void {
    this.routeSubscription = this.route.params.subscribe((params: Params) => {
      this.id = params.id;
      if (params.id !== undefined) {
        this.initEditForm();
      }
    });
  }

  initEditForm() {
    this.gameSubscription = this.store
      .select('games')
      .pipe(
        map((gameState) => gameState.games.find((game) => game.id === this.id))
      )
      .subscribe((gameData) => {
        setTimeout(() => {
          if (this.gameForm && gameData) {
            this.manageMode = 'update';
            this.requestMethod = 'patch';
            this.gameForm.setValue({
              name: gameData.name,
              ownerCompany: gameData.ownerCompany,
              description: gameData.description,
              imageUrl: gameData.imageUrl,
              trailerUrl: gameData.trailerUrl,
            });
          }
        }, 0);
      });
  }

  getErrorMessage(): string {
    return '';
  }

  onSubmit(): void {
    const gameData: Game = this.gameForm.value as Game;

    this.store.dispatch(
      new GamesActions.ManageGameStarted({
        game: { ...gameData, id: this.id },
        mode: this.manageMode,
        requestMethod: this.requestMethod,
      })
    );
  }

  onTogglePreview(): void {
    this.isPreviewMode = !this.isPreviewMode;
  }

  ngOnDestroy(): void {
    if (this.gameSubscription) {
      this.gameSubscription.unsubscribe();
    }
    if (this.routeSubscription) {
      this.routeSubscription.unsubscribe();
    }
  }
}
