import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Store } from '@ngrx/store';
import { Subscription } from 'rxjs';
import { map } from 'rxjs/operators';
import { User } from 'src/app/auth/users.model';
import { AppState } from 'src/app/store/app.reducer';
import { Game } from '../games.model';
import * as GameActions from '../store/games.actions';

@Component({
  selector: 'app-game-item',
  templateUrl: './game-item.component.html',
  styleUrls: ['./game-item.component.scss'],
})
export class GameItemComponent implements OnInit {
  @Input() index: number;
  isReviewDialogOpen: boolean = false;
  gameSub: Subscription;
  userSub: Subscription;
  user: User;

  game: Game;
  content: string;
  isFavGame: boolean;
  isTryGame: boolean;

  isMobile = this.breakpointObserver.observe(Breakpoints.Handset).pipe(
    map(({ matches }) => {
      if (matches) {
        return true;
      }
      return false;
    })
  );

  constructor(
    private breakpointObserver: BreakpointObserver,
    private store: Store<AppState>,
    private router: Router,
    private route: ActivatedRoute
  ) {}

  ngOnInit(): void {
    this.gameSub = this.store.select('games').subscribe((state) => {
      this.game = state.games[this.index];
      this.content = this.game.description.substr(0, 80) + '...';
      this.isFavGame = !!state.favGames.find(
        (game) => game.id === this.game.id
      );
      this.isTryGame = !!state.tryGames.find(
        (game) => game.id === this.game.id
      );
    });
    this.userSub = this.store.select('auth').subscribe((state) => {
      this.user = state.user;
    });
  }

  onChangeFavStatus() {
    const action = this.isFavGame
      ? new GameActions.RemoveFavGameStarted(this.game.id)
      : new GameActions.AddFavGameStarted(this.game.id);

    this.store.dispatch(action);
  }

  onChangeTryStatus() {
    const action = this.isTryGame
      ? new GameActions.RemoveTryGameStarted(this.game.id)
      : new GameActions.AddTryGameStarted(this.game.id);

    this.store.dispatch(action);
  }

  onAddReview(gameId: string) {
    this.isReviewDialogOpen = this.game.id === gameId;
  }

  onCloseReviewDialog() {
    this.isReviewDialogOpen = false;
  }

  onSelectGame(id: string) {
    this.router.navigate([id], { relativeTo: this.route });
  }

  onEditGame(id: string) {
    this.router.navigate([`${id}/edit`], { relativeTo: this.route });
  }

  ngOnDestroy(): void {
    if (this.gameSub) {
      this.gameSub.unsubscribe();
    }
    if (this.userSub) {
      this.userSub.unsubscribe();
    }
    this.onCloseReviewDialog();
  }
}
