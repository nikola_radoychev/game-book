export interface Review {
  id: string;
  content: string;
  creator: string;
}
