import { Component, OnDestroy, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { Subscription } from 'rxjs';
import { AppState } from '../store/app.reducer';
import * as ErrorsActions from './store/errors.actions';

@Component({
  selector: 'app-errors',
  templateUrl: './errors.component.html',
  styleUrls: ['./errors.component.scss'],
})
export class ErrorsComponent implements OnInit, OnDestroy {
  message: string;
  subscription: Subscription;
  constructor(private store: Store<AppState>) {}
  ngOnInit(): void {
    this.subscription = this.store.select('errors').subscribe((state) => {
      this.message = state.errorMessage;
    });
  }

  onDismissError() {
    this.store.dispatch(new ErrorsActions.ClearError());
  }

  ngOnDestroy(): void {
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
  }
}
