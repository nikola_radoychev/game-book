import { Injectable } from '@angular/core';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { tap } from 'rxjs/operators';
import * as ErrorsActions from './errors.actions';

@Injectable()
export class ErrorsEffects {
  @Effect({ dispatch: false })
  clearError = this.actions.pipe(
    ofType(ErrorsActions.CLEAR_ERROR),
    tap((data: ErrorsActions.ClearError) => {})
  );

  constructor(private actions: Actions) {}
}
