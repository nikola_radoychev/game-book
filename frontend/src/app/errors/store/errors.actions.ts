import { Action } from '@ngrx/store';

export const SET_ERROR = '[Errors] Set Error';
export class SetError implements Action {
  readonly type: string = SET_ERROR;
  constructor(public payload: string) {}
}

export const CLEAR_ERROR = '[Errors] Clear Error';
export class ClearError implements Action {
  readonly type: string = CLEAR_ERROR;
}

export type ErrorsActions = ClearError | SetError;
