import * as ErrorsActions from './errors.actions';

export interface State {
  errorMessage: string;
}

const initialState: State = {
  errorMessage: null,
};

export const errorReducer = (
  state: State = initialState,
  action: any
): State => {
  switch (action.type) {
    case ErrorsActions.SET_ERROR: {
      return {
        ...state,
        errorMessage: action.payload,
      };
    }
    case ErrorsActions.CLEAR_ERROR: {
      return {
        ...state,
        errorMessage: null,
      };
    }
    default:
      return state;
  }
};
