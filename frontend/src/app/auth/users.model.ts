export type UserRole = 'Admin' | 'Moderator' | 'User';

export class User {
  constructor(
    public name: string,
    public email: string,
    public id: string,
    public role: UserRole,
    public token: string
  ) {}
}
