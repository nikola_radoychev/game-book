import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { of } from 'rxjs';
import { catchError, map, switchMap, tap } from 'rxjs/operators';
import { mapDbToUser } from 'src/app/mappers/users';
import { User, UserRole } from '../users.model';
import * as AuthActions from './auth.actions';
import * as ErrorsActions from '../../errors/store/errors.actions';

export interface UserResponseData {
  name: string;
  _id: string;
  email: string;
  role: UserRole;
}

export interface AuthResponseData {
  token: string;
}

const handleSignUp = (loginData: { email: string; password: string }) => {
  return new AuthActions.LoginStart(loginData);
};

const handleAuthentication = (user: User) => {
  localStorage.setItem('tokenData', JSON.stringify({ token: user.token }));
  return new AuthActions.AuthenticateSuccess(user);
};

const handleAuthorization = (authData: AuthResponseData) => {
  return new AuthActions.AuthorizationSuccess(authData.token);
};

const handleError = (errorRes) => {
  const errorMessage = 'Server operation failed!';
  if (
    (!errorRes.error && !errorRes.error.error) ||
    (typeof errorRes.error !== 'string' && !errorRes.error.error)
  ) {
    return of(new ErrorsActions.SetError(errorMessage));
  }
  return of(
    new ErrorsActions.SetError(errorRes.error?.error || errorRes.error)
  );
};

@Injectable()
export class AuthEffects {
  @Effect()
  signUp = this.actions.pipe(
    ofType(AuthActions.SIGN_UP_START),
    switchMap((signUpData: AuthActions.SignUpStart) => {
      return this.http
        .post('http://localhost:5050/rest/users/create', {
          ...signUpData.payload,
        })
        .pipe(
          map(() => handleSignUp(signUpData.payload)),
          catchError(handleError)
        );
    })
  );

  @Effect()
  authLogin = this.actions.pipe(
    ofType(AuthActions.LOGIN_START),
    switchMap((authData: AuthActions.LoginStart) => {
      return this.http
        .post<AuthResponseData>('http://localhost:5050/rest/auth/login', {
          ...authData.payload,
        })
        .pipe(map(handleAuthorization), catchError(handleError));
    })
  );

  @Effect()
  authorization = this.actions.pipe(
    ofType(AuthActions.AUTHORIZATION_SUCCESS),
    switchMap((data: AuthActions.AuthorizationSuccess) => {
      return this.http.get('http://localhost:5050/rest/auth/profile').pipe(
        map((resData: UserResponseData) =>
          handleAuthentication(mapDbToUser(resData, data.payload))
        ),
        catchError(handleError)
      );
    })
  );

  @Effect({ dispatch: false })
  authRedirect = this.actions.pipe(
    ofType(AuthActions.AUTHENTICATE_SUCCESS),
    tap((_: AuthActions.AuthenticateSuccess) => {
      this.router.navigate(['/']);
    })
  );

  @Effect()
  autoLogin = this.actions.pipe(
    ofType(AuthActions.AUTO_LOGIN),
    map(() => {
      const tokenData: { token: string } = JSON.parse(
        localStorage.getItem('tokenData')
      );
      if (!tokenData) {
        return { type: 'DUMMY' };
      }
      return handleAuthorization(tokenData);
    }),
    catchError(handleError)
  );

  @Effect({ dispatch: false })
  authLogout = this.actions.pipe(
    ofType(AuthActions.LOGOUT),
    tap(() => {
      localStorage.removeItem('tokenData');

      this.router.navigate(['/auth']);
    })
  );

  constructor(
    private actions: Actions,
    private http: HttpClient,
    private router: Router
  ) {}
}
