import { User } from '../users.model';
import * as AuthActions from './auth.actions';

export interface State {
  user: User;
  isLoading: boolean;
}

const initialState: State = {
  user: null,
  isLoading: false,
};

export const authReducer = (
  state: State = initialState,
  action: any
): State => {
  switch (action.type) {
    case AuthActions.LOGIN_START: {
      return {
        ...state,
        isLoading: true,
      };
    }
    case AuthActions.AUTHENTICATE_SUCCESS: {
      return {
        ...state,
        user: action.payload,
        isLoading: false,
      };
    }
    case AuthActions.AUTHORIZATION_SUCCESS: {
      return {
        ...state,
        user: {
          ...state.user,
          token: action.payload,
        },
      };
    }
    case AuthActions.LOGOUT:
      return { ...state, user: null };
    default:
      return state;
  }
};
