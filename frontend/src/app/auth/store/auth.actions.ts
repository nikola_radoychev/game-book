import { Action } from '@ngrx/store';
import { User } from '../users.model';

export const LOGIN_START = '[Auth] Login start';
export class LoginStart implements Action {
  readonly type: string = LOGIN_START;
  constructor(public payload: { email: string; password: string }) {}
}

export const SIGN_UP_START = '[Auth] Sign up start';
export class SignUpStart implements Action {
  readonly type: string = SIGN_UP_START;
  constructor(
    public payload: {
      name: string;
      email: string;
      password: string;
      repeatPassword: string;
    }
  ) {}
}

export const LOGOUT = '[Auth] Logout';
export class Logout implements Action {
  readonly type: string = LOGOUT;
}
export const AUTHORIZATION_SUCCESS = '[Auth] Authorization success';
export class AuthorizationSuccess implements Action {
  readonly type: string = AUTHORIZATION_SUCCESS;
  constructor(public payload: string) {}
}

export const AUTHENTICATE_SUCCESS = '[Auth] Authenticate success';
export class AuthenticateSuccess implements Action {
  readonly type = AUTHENTICATE_SUCCESS;
  constructor(public payload: User) {}
}

export const AUTO_LOGIN = '[Auth] Auto login';
export class AutoLogin implements Action {
  readonly type = AUTO_LOGIN;
}

export type AuthActions =
  | LoginStart
  | SignUpStart
  | Logout
  | AuthorizationSuccess
  | AuthenticateSuccess
  | AutoLogin;
