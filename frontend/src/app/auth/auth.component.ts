import { Component, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Store } from '@ngrx/store';
import { Subscription } from 'rxjs';

import * as AuthActions from './store/auth.actions';
import { AppState } from '../store/app.reducer';

@Component({
  selector: 'app-auth',
  templateUrl: './auth.component.html',
  styleUrls: ['./auth.component.scss'],
})
export class AuthComponent implements OnInit {
  @ViewChild('f', { static: false }) authForm: NgForm;
  signInMode: boolean = true;
  hidePassword: boolean = true;
  modeSub: Subscription;

  constructor(private store: Store<AppState>) {}

  ngOnInit(): void {}

  onSubmit() {
    if (!this.authForm.valid) {
      return;
    }
    const { name, email, password, repeatPassword } = this.authForm.value;
    if (this.signInMode) {
      this.store.dispatch(
        new AuthActions.LoginStart({
          email,
          password,
        })
      );
    } else {
      this.store.dispatch(
        new AuthActions.SignUpStart({
          name,
          email,
          password,
          repeatPassword,
        })
      );
    }

    this.authForm.reset();
  }

  onSwitchMode(): void {
    this.signInMode = !this.signInMode;
    this.authForm.reset();
  }

  onRepeatPassword() {
    if (
      this.authForm.value.repeatPassword &&
      this.authForm.value.repeatPassword !== '' &&
      this.authForm.value.repeatPassword !== this.authForm.value.password
    ) {
      this.authForm.controls['repeatPassword'].setErrors({ notMatch: true });
    }
  }

  getErrorMessage(type: string): string {
    if (
      this.authForm.controls[type]?.errors &&
      this.authForm.controls[type]?.touched
    ) {
      if (this.authForm.controls[type].errors.minlength) {
        return `The ${type} should contains at least 6 symbols!`;
      }
      if (this.authForm.controls[type].errors.email) {
        return 'Not a valid email!';
      }
      if (this.authForm.controls[type].errors.pattern) {
        return `The ${type} should contains 1 digit and 1 letter at least`;
      }
      if (this.authForm.controls[type].errors.notMatch) {
        return 'Password not much';
      }
      return 'This field is required!';
    }
  }
}
