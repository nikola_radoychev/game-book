import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-about',
  templateUrl: './about.component.html',
  styleUrls: ['./about.component.scss'],
})
export class AboutComponent implements OnInit {
  selectedPage: number = 1;

  constructor() {}

  ngOnInit(): void {}

  getPageTitle(): string {
    switch (this.selectedPage) {
      case 1: {
        return 'Modules';
      }
      case 2: {
        return 'Components';
      }
      case 3: {
        return 'NgRx Store';
      }
      case 4: {
        return 'Others';
      }
      case 5: {
        return 'User roles';
      }
      default:
        return '';
    }
  }

  goToNextPage(): void {
    if (this.selectedPage === 5) {
      this.selectedPage = 1;
    } else {
      this.selectedPage++;
    }
  }

  goToPrevPage(): void {
    if (this.selectedPage === 1) {
      this.selectedPage = 5;
    } else {
      this.selectedPage--;
    }
  }
}
