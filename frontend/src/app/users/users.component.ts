import { Component, OnDestroy, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { TableService } from 'src/app/shared/table/table.service';
import { AppState } from 'src/app/store/app.reducer';
import * as UsersActions from './store/users.actions';
import { User, UserRole } from '../auth/users.model';
import { Subscription } from 'rxjs';

export interface UsersTable extends User {
  action: UsersActions.UsersActions;
}

export const userTableColumns = ['id', 'name', 'email', 'role', 'action'];
@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.scss'],
})
export class UsersComponent implements OnInit, OnDestroy {
  dataSource: any;
  tableName = 'Manage Users';
  displayedColumns = userTableColumns;
  loggedInUser: User;
  users: Array<User>;
  authSub: Subscription;
  userSub: Subscription;

  constructor(private store: Store<AppState>) {}

  ngOnInit(): void {
    this.store.dispatch(new UsersActions.GetUsersStarted());

    setTimeout(() => {
      this.userSub = this.store.select('users').subscribe((state) => {
        this.dataSource = state.users.map((user) => ({
          ...user,
          action: new UsersActions.GetUsersStarted(),
        }));
        this.users = state.users;
      });
      this.authSub = this.store.select('auth').subscribe((state) => {
        this.loggedInUser = state.user;
      });
    }, 0);
  }

  onDeleteUser(id: string) {
    this.store.dispatch(new UsersActions.DeleteUserStarted(id));
  }

  onChangeUserRole(reqData: { role: UserRole; id: string }) {
    this.store.dispatch(new UsersActions.ChangeUserRoleStarted(reqData));
  }
  ngOnDestroy(): void {
    if (this.authSub) {
      this.authSub.unsubscribe();
    }
    if (this.userSub) {
      this.userSub.unsubscribe();
    }
  }
}
