import { User } from 'src/app/auth/users.model';
import * as UsersActions from './users.actions';

export interface State {
  users: Array<User>;
  isLoading: boolean;
}

const initialState: State = {
  users: [],
  isLoading: false,
};

export const usersReducer = (
  state: State = initialState,
  action: UsersActions.UsersActions
): State => {
  switch (action.type) {
    case UsersActions.GET_USERS_STARTED:
    case UsersActions.CHANGE_USER_ROLE_STARTED:
    case UsersActions.DELETE_USER_STARTED: {
      return {
        ...state,
        isLoading: true,
      };
    }
    case UsersActions.GET_USERS_SUCCEEDED: {
      return { ...state, isLoading: false, users: action.payload };
    }
    default:
      return state;
  }
};
