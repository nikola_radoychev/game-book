import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { of } from 'rxjs';
import { catchError, map, switchMap } from 'rxjs/operators';
import { UserResponseData } from 'src/app/auth/store/auth.effects';
import { mapDbToUser } from 'src/app/mappers/users';
import * as ErrorsActions from '../../errors/store/errors.actions';
import * as UsersActions from './users.actions';

const handleError = (errorRes) => {
  const errorMessage = 'Server operation failed!';
  if (
    (!errorRes.error && !errorRes.error.error) ||
    (typeof errorRes.error !== 'string' && !errorRes.error.error)
  ) {
    return of(new ErrorsActions.SetError(errorMessage));
  }
  return of(
    new ErrorsActions.SetError(errorRes.error?.error || errorRes.error)
  );
};
@Injectable()
export class UsersEffects {
  @Effect()
  loadUsers = this.actions.pipe(
    ofType(UsersActions.GET_USERS_STARTED),
    switchMap((_: UsersActions.GetUsersStarted) => {
      return this.http.get('http://localhost:5050/rest/users/all').pipe(
        map((res: Array<UserResponseData>) => {
          const result = res.map((user) => mapDbToUser(user, ''));
          return new UsersActions.GetUsersSucceeded(result);
        }),
        catchError(handleError)
      );
    })
  );

  @Effect()
  changeUserRole = this.actions.pipe(
    ofType(UsersActions.CHANGE_USER_ROLE_STARTED),
    switchMap((requestData: UsersActions.ChangeUserRoleStarted) => {
      return this.http
        .patch(
          'http://localhost:5050/rest/users/changerole',
          requestData.payload
        )
        .pipe(
          map(() => {
            return new UsersActions.GetUsersStarted();
          }),
          catchError(handleError)
        );
    })
  );

  @Effect()
  deleteUser = this.actions.pipe(
    ofType(UsersActions.DELETE_USER_STARTED),
    switchMap((requestData: UsersActions.DeleteUserStarted) => {
      return this.http
        .delete(`http://localhost:5050/rest/users/${requestData.payload}`)
        .pipe(
          catchError(() => {
            return of();
          }),
          map(() => {
            return new UsersActions.GetUsersStarted();
          })
        );
    })
  );

  constructor(private actions: Actions, private http: HttpClient) {}
}
