import { Action } from '@ngrx/store';
import { User, UserRole } from 'src/app/auth/users.model';

export const GET_USERS_STARTED = '[Users] Get Users Started';
export class GetUsersStarted implements Action {
  readonly type = GET_USERS_STARTED;
}

export const GET_USERS_SUCCEEDED = '[Users] Get Users Succeeded';
export class GetUsersSucceeded implements Action {
  readonly type = GET_USERS_SUCCEEDED;
  constructor(public payload: Array<User>) {}
}

export const CHANGE_USER_ROLE_STARTED = '[Users] Change user role started';
export class ChangeUserRoleStarted implements Action {
  readonly type = CHANGE_USER_ROLE_STARTED;
  constructor(public payload: { role: UserRole; id: string }) {}
}

export const DELETE_USER_STARTED = '[Users] Delete user started';
export class DeleteUserStarted implements Action {
  readonly type = DELETE_USER_STARTED;
  constructor(public payload: string) {}
}

export type UsersActions =
  | GetUsersStarted
  | GetUsersSucceeded
  | ChangeUserRoleStarted
  | DeleteUserStarted;
