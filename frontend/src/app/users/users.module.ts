import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { AuthAdminGuard } from '../auth/guards/auth.admin.guard';
import { SharedModule } from '../shared/shared.module';
import { UsersComponent } from './users.component';

@NgModule({
  declarations: [UsersComponent],
  imports: [
    RouterModule.forChild([
      {
        path: '',
        component: UsersComponent,
        canActivate: [AuthAdminGuard],
      },
    ]),
    SharedModule,
  ],
  exports: [],
})
export class UsersModule {}
