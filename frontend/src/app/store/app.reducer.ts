import { ActionReducerMap } from '@ngrx/store';

import * as fromAuth from '../auth/store/auth.reducer';
import * as fromGames from '../games/store/games.reducer';
import * as fromUsers from '../users/store/users.reducer';
import * as fromErrors from '../errors/store/errors.reducer';

export interface AppState {
  auth: fromAuth.State;
  games: fromGames.State;
  users: fromUsers.State;
  errors: fromErrors.State;
}

export const appReducer: ActionReducerMap<AppState> = {
  auth: fromAuth.authReducer,
  games: fromGames.gamesReducer,
  users: fromUsers.usersReducer,
  errors: fromErrors.errorReducer,
};
