import { OnInit, Component, Input, OnChanges } from '@angular/core';

@Component({
  selector: 'app-youtube-player',
  styleUrls: ['youtube-player.component.scss'],
  templateUrl: 'youtube-player.component.html',
})
export class YoutubePlayerComponent implements OnInit, OnChanges {
  @Input() videoUrl;
  videoId: string;
  private apiLoaded: boolean = false;
  ngOnInit() {
    if (this.videoUrl) {
      this.videoId = this.videoUrl.split('v=')[1];
    }
    if (!this.apiLoaded) {
      // This code loads the IFrame Player API code asynchronously, according to the instructions at
      // https://developers.google.com/youtube/iframe_api_reference#Getting_Started
      const tag = document.createElement('script');
      tag.src = 'https://www.youtube.com/iframe_api';
      document.body.appendChild(tag);
      this.apiLoaded = true;
    }
  }

  ngOnChanges(): void {
    if (this.videoUrl) {
      this.videoId = this.videoUrl.split('v=')[1];
    }
  }
}
