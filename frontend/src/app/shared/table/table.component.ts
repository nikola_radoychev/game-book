import { Component, OnDestroy, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { Subscription } from 'rxjs';
import { GamesTable } from 'src/app/games/games-table/games.table.component';
import { AppState } from 'src/app/store/app.reducer';
import { UsersTable } from 'src/app/users/users.component';
import { TableService } from './table.service';

export type TableData = { position: string } | GamesTable | UsersTable;

@Component({
  selector: 'app-table',
  styleUrls: ['table.component.scss'],
  templateUrl: 'table.component.html',
})
export class TableComponent implements OnInit, OnDestroy {
  constructor(
    private tableService: TableService,
    private store: Store<AppState>
  ) {}
  dataSource: Array<TableData> = [];
  displayedColumns: Array<string> = [];
  tableName: string = '';

  subscription: Subscription;
  sourceSub: Subscription;

  ngOnInit(): void {
    this.subscription = this.tableService.tableInputChanged.subscribe(
      (newData) => {
        this.dataSource = newData.tableContent;
        this.displayedColumns = newData.displayedColumns;
        this.tableName = newData.tableName;
      }
    );
  }

  onActionCall(action: any) {
    this.store.dispatch(action);
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }
}
