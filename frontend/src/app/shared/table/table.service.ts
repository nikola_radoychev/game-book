import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import { TableData } from './table.component';

interface TableServiceInput {
  tableContent: Array<TableData>;
  displayedColumns: Array<string>;
  tableName: string;
}
@Injectable({ providedIn: 'root' })
export class TableService {
  tableInputChanged = new Subject<TableServiceInput>();

  tableInputData: TableServiceInput = {
    tableContent: [],
    displayedColumns: [],
    tableName: '',
  };

  setTableData(
    tableContent: Array<TableData>,
    displayedColumns: Array<string>,
    tableName: string
  ): void {
    this.tableInputData = { tableContent, displayedColumns, tableName };
    this.tableInputChanged.next({ ...this.tableInputData });
  }

  clearTableData(): void {
    this.tableInputData = {
      tableContent: [],
      displayedColumns: [],
      tableName: '',
    };
  }
}
