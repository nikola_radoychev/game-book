import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TableComponent } from './table/table.component';
import { MaterialModule } from './material.module';
import { YouTubePlayerModule } from '@angular/youtube-player';
import { YoutubePlayerComponent } from './youtube-player/youtube-player.component';
import { ModalComponent } from './modal/modal.component';
import { ErrorsComponent } from '../errors/errors.component';

@NgModule({
  declarations: [
    TableComponent,
    YoutubePlayerComponent,
    ModalComponent,
    ErrorsComponent,
  ],
  imports: [CommonModule, MaterialModule, YouTubePlayerModule],
  exports: [
    CommonModule,
    MaterialModule,
    TableComponent,
    YouTubePlayerModule,
    YoutubePlayerComponent,
    ModalComponent,
    ErrorsComponent,
  ],
})
export class SharedModule {}
