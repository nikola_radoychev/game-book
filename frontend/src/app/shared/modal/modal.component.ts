import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-modal',
  templateUrl: './modal.component.html',
  styleUrls: ['./modal.component.scss'],
})
export class ModalComponent implements OnInit {
  @Input() isDialogOpen: boolean = false;
  @Input() title: string = '';
  @Output() dialogClosed = new EventEmitter<void>();
  subscription: Subscription;
  isOpen: boolean = false;
  constructor() {}

  ngOnInit(): void {}

  onCloseModal(): void {
    this.dialogClosed.emit();
  }

  ngOnDestroy(): void {
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
  }
}
